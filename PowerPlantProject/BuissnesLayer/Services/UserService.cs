﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.Mappers;
using PowerPlantProject;
using PowerPlantProject.Repository;

namespace BuissnesLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IGenericRepository<User> _userRepo;

        public UserService(IGenericRepository<User> userRepo)
        {
            _userRepo = userRepo;
        }

        public void AddUser(UserDto userDto)
        {
            var user = FromDtoToEntity.UserDtoModelToEntity(userDto);
            _userRepo.Add(user);
            _userRepo.SaveChanges();
        }

        public bool DoesUserExist(UserDto userDto)
        {
            var user = FromDtoToEntity.UserDtoModelToEntity(userDto);
            foreach (var userData in _userRepo.GetAll())
            {
                if (user.UserName != null && 
                    user.UserName == userData.UserName && 
                    user.Password == userData.Password)
                {
                    return true;
                }
            }
            return false;
        }

        public bool DoesUserLoginExistInDb(string inputedLogin)
        {
            var namesList = _userRepo.GetAll().Select(e => e.UserName).ToList();
            if (namesList.Contains(inputedLogin))
            {
                return true;
            }
            return false;
        }

        public bool DoesUserIsAdmin(UserDto user)
        {
            foreach (var userInDb in _userRepo.GetAll())
            {
                if (userInDb.UserName == user.UserName && userInDb.IsAdmin)
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetValidAdminPassword(string inputedPassword, string loggedUserPassword)
        {
            foreach (var user in _userRepo.GetAll())
            {
                if (user.Password == inputedPassword && loggedUserPassword == inputedPassword)
                {
                    return true;
                }
            }
            return false;
        }

        public bool DoesUserExistInDataBase(string inputedUserName)
        {
            foreach (var user in _userRepo.GetAll())
            {
                if (user.UserName != null && user.UserName == inputedUserName)
                {
                    return true;
                }
            }
            return false;
        }

        public bool DeleteUser(string userName)
        {
            foreach (var user in _userRepo.GetAll())
            {
                if (user.UserName == userName)
                {
                    _userRepo.Delete(user.Id);
                    _userRepo.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        
    }
}