﻿using System;
using System.Collections.Generic;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.JsonManagment;
using BuissnesLayer.Mappers;
using DataLayer.Models;
using PowerPlantProject.Repository;

namespace BuissnesLayer.Services
{
    public class DeviceInformationService : IDeviceInformationService
    {
        private readonly IGenericRepository<DeviceInformation> _deviceRepo;

        public DeviceInformationService(IGenericRepository<DeviceInformation> deviceRepo)
        {
            _deviceRepo = deviceRepo;
        }

        public List<DeviceInformationDto> GetAllAlerts(DateTime initialTime, DateTime lastTime)
        {
            var alertList = new List<DeviceInformationDto>();
            var deviceInfoDto = new DeviceInformationDto();

            foreach (var info in _deviceRepo.GetAll())
            {
                if (info == null)
                    return null;

                deviceInfoDto = FromEntityToDto.DeviceInformationEntityModelToDto(info);

                if (deviceInfoDto.OverloadDateTime > initialTime && deviceInfoDto.OverloadDateTime < lastTime)
                {
                    alertList.Add(deviceInfoDto);
                }
            }

            return alertList;
        }
        public void ExportOverloadReport(DateTime dateSince, DateTime dateTo, string fileName)
        {
            var listToExport = GetAllAlerts(dateSince, dateTo);

            JsonService<DeviceInformationDto> jsonService = new JsonService<DeviceInformationDto>();

            jsonService.SaveDataListToJson(listToExport,fileName);

        }

    }
}