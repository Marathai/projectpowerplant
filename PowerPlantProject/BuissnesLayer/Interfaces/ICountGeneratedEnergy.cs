﻿namespace BuissnesLayer
{
    public interface ICountGeneratedEnergy
    {
        double EnergyGenerated(int timePeriod, double powerAlreadyGenerated ,double powerCurrentlyGenerated);
    }
}