﻿using BuissnesLayer.DTOs;

namespace BuissnesLayer.Interfaces
{
    public interface IUserService
    {
        void AddUser(UserDto userDto);
        bool DoesUserExist(UserDto userDto);
        bool DoesUserExistInDataBase(string inputedUserName);
        bool DeleteUser(string userNameToDelete);
        bool DoesUserIsAdmin(UserDto user);
        bool GetValidAdminPassword(string inputedPassword, string loggedUserPassword);
        bool DoesUserLoginExistInDb(string inputedLogin);

    }
}