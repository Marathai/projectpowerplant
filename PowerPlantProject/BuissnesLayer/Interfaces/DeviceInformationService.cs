﻿using System;
using System.Collections.Generic;
using BuissnesLayer.DTOs;

namespace BuissnesLayer.Interfaces
{
    public interface IDeviceInformationService
    {
        List<DeviceInformationDto> GetAllAlerts(DateTime initialTime, DateTime lastTime);
        void ExportOverloadReport(DateTime dateSince, DateTime dateTo, string fileName);
    }
}