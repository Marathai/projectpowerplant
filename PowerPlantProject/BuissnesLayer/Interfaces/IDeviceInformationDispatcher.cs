using System;
using System.Threading;
using BuissnesLayer.DTOs;

namespace BuissnesLayer
{
    public interface IDeviceInformationDispatcher
    {
        //void GetNewConditions(int refreshFrq, double delta);
        void DevicesCurrentState(double percentage, UserDto user);
        void SaveOverloadedLoop(double percentage, UserDto loggedUser);
        void WhenVolumenOverloaded(string turpineName, string parameterName, DateTime currentTime, UserDto loggedUser);
        //UserDto GetLoggedUserInfo(UserDto loggedUser);

        void SetLoggedUser(UserDto user);
    }
}