﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Xml;
using BuissnesLayer.Configuration;
using BuissnesLayer.DTOs;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace BuissnesLayer.JsonManagment
{
    public class JsonService<T> 
    {
        public void SaveDataToJson(T jsonData, string FileName)
        {
            string output = JsonConvert.SerializeObject(jsonData,Formatting.Indented);
            File.WriteAllText(FileName + ".json", output);
        }

        public void SaveDataListToJson(List<T> jsonDataList, string fileName)
        {
            string output = JsonConvert.SerializeObject(jsonDataList, Formatting.Indented);
            File.WriteAllText(fileName + ".json", output);
        }

        public ConfigurationClass GetDataFromJson()
        {
            var jObj = JsonConvert.DeserializeObject<ConfigurationClass>(File.ReadAllText("settings.json"));
            var configClass = new ConfigurationClass
            {
                PercentOfVarationFromTypicalValue = jObj.PercentOfVarationFromTypicalValue,
                RefreshDataInterval = jObj.RefreshDataInterval
            };

            return configClass;
        }
    }
}