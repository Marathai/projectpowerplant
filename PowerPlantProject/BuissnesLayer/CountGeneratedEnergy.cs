﻿namespace BuissnesLayer
{
    public class CountGeneratedEnergy : ICountGeneratedEnergy
    {
        public double EnergyGenerated(int timePeriod, double powerAlreadyGenerated ,double powerCurrentlyGenerated)
        {
            powerAlreadyGenerated += (powerCurrentlyGenerated / (3600 * (1000 / timePeriod)));
            return powerAlreadyGenerated;
        }
    }
}