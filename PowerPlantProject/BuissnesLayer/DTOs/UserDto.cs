﻿namespace BuissnesLayer.DTOs
{
    public class UserDto
    {
        public int Id;
        public string UserName;
        public string Password;
        public bool IsAdmin;
    }
}