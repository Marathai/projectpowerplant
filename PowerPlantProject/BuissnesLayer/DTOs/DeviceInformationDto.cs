﻿using System;
using PowerPlantProject;

namespace BuissnesLayer.DTOs
{
    public class DeviceInformationDto
    {
        public int Id;
        public string DeviceName;
        public string ParameterNameWhenOverload;
        public DateTime OverloadDateTime;
        public string LoggedUserInformation;
    }
}