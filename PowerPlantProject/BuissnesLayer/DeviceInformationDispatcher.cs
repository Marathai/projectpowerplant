﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BuissnesLayer.DTOs;
using BuissnesLayer.Mappers;
using DataLayer.Models;
using PowerPlantDataProvider;
using PowerPlantProject;
using PowerPlantProject.Repository;

namespace BuissnesLayer
{
    public class DeviceInformationDispatcher : IDeviceInformationDispatcher
    {
        private readonly IGenericRepository<DeviceInformation> _deviceRepo;
        private readonly IGenericRepository<User> _userRepo;

        public DeviceInformationDispatcher(IGenericRepository<DeviceInformation> deviceRepo,
            IGenericRepository<User> userRepo)
        {
            _userRepo = userRepo;
            _deviceRepo = deviceRepo;
        }

        private UserDto _loggedUser;

        public void SaveOverloadedLoop(double percentage, UserDto loggedUser)
        {
            
            DevicesCurrentState(percentage, loggedUser);
        }

        public void SetLoggedUser(UserDto user)
        {
            _loggedUser = user;
        }


        public void DevicesCurrentState(double percentage, UserDto loggedUser)
        {
            Dictionary<string, bool> turbinesParams = new Dictionary<string, bool>();
            Dictionary<string, bool> cauldronsParams = new Dictionary<string, bool>();
            Dictionary<string, bool> transistorsParams = new Dictionary<string, bool>();
            var sth = PowerPlant.Instance;
            double maxValue;
            double minValue;
            var currentTime = DateTime.Now;

            while (true)
            {
                foreach (var turbine in sth.Turbines)
                {
                    foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(turbine))
                    {
                        var value = property.GetValue(turbine);
                        if (value.GetType() == typeof(AssetParameter))
                        {
                            AssetParameter ap = (AssetParameter) value;

                            var currentValue = ap.CurrentValue;

                            if (!turbinesParams.Keys.Contains(property.Name + turbine.Name))
                            {
                                turbinesParams.Add(property.Name + turbine.Name, false);
                            }

                            if (percentage != 0) //jest json
                            {
                                maxValue = ap.TypicalValue * (1 + percentage);
                                minValue = ap.TypicalValue * (1 - percentage);

                                if (currentValue > maxValue || currentValue < minValue)
                                {
                                    var wasAlertBefore = turbinesParams[property.Name + turbine.Name];

                                    if (!wasAlertBefore)
                                    {
                                        currentTime = DateTime.Now;
                                        WhenVolumenOverloaded(turbine.Name, property.Name, currentTime, loggedUser);
                                    }
                                    turbinesParams[property.Name + turbine.Name] = true;
                                }
                                else
                                {
                                    turbinesParams[property.Name + turbine.Name] = false;
                                }

                            }
                            else if (percentage == 0) //nie ma jsona
                            {
                                maxValue = ap.MaxValue;
                                minValue = ap.MinValue;

                                if (currentValue > maxValue || currentValue < minValue)
                                {
                                    var wasAlertBefore = turbinesParams[property.Name + turbine.Name];

                                    if (!wasAlertBefore)
                                    {
                                        currentTime = DateTime.Now;
                                        WhenVolumenOverloaded(turbine.Name, property.Name, currentTime, loggedUser);
                                    }
                                    turbinesParams[property.Name + turbine.Name] = true;
                                }
                                else
                                {
                                    turbinesParams[property.Name + turbine.Name] = false;
                                }
                            }
                        }
                    }
                    foreach (var cauldron in sth.Cauldrons)
                    {
                        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(cauldron))
                        {
                            var value = property.GetValue(cauldron);
                            if (value.GetType() == typeof(AssetParameter))
                            {
                                AssetParameter ap = (AssetParameter)value;

                                var currentValue = ap.CurrentValue;

                                if (!cauldronsParams.Keys.Contains(property.Name + cauldron.Name))
                                {
                                    cauldronsParams.Add(property.Name + cauldron.Name, false);
                                }
                                if (percentage != 0) //jest json
                                {
                                    maxValue = ap.TypicalValue * (1 + percentage);
                                    minValue = ap.TypicalValue * (1 - percentage);

                                    if (currentValue > maxValue || currentValue < minValue)
                                    {
                                        var wasAlertBefore = cauldronsParams[property.Name + cauldron.Name];

                                        if (!wasAlertBefore)
                                        {
                                            currentTime = DateTime.Now;
                                            WhenVolumenOverloaded(cauldron.Name, property.Name, currentTime, loggedUser);
                                        }
                                        cauldronsParams[property.Name + cauldron.Name] = true;
                                    }
                                    else
                                    {
                                        cauldronsParams[property.Name + cauldron.Name] = false;
                                    }
                                }
                                else //nie ma jsona
                                {
                                    maxValue = ap.MaxValue;
                                    minValue = ap.MinValue;

                                    if (currentValue > maxValue || currentValue < minValue)
                                    {
                                        var wasAlertBefore = cauldronsParams[property.Name + cauldron.Name];

                                        if (!wasAlertBefore)
                                        {
                                            currentTime = DateTime.Now;
                                            WhenVolumenOverloaded(cauldron.Name, property.Name, currentTime, loggedUser);
                                        }
                                        cauldronsParams[property.Name + cauldron.Name] = true;
                                    }
                                    else
                                    {
                                        cauldronsParams[property.Name + cauldron.Name] = false;
                                    }
                                }
                            }
                        }
                    }
                    foreach (var trans in sth.Transformators)
                    {
                        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(trans))
                        {
                            var value = property.GetValue(trans);
                            if (value.GetType() == typeof(AssetParameter))
                            {
                                AssetParameter ap = (AssetParameter)value;

                                var currentValue = ap.CurrentValue;

                                if (!transistorsParams.Keys.Contains(property.Name + trans.Name))
                                {
                                    transistorsParams.Add(property.Name + trans.Name, false);
                                }

                                if (percentage != 0) //jest json
                                {
                                    maxValue = ap.TypicalValue * (1 + percentage);
                                    minValue = ap.TypicalValue * (1 - percentage);

                                    if (currentValue > maxValue || currentValue < minValue)
                                    {
                                        var wasAlertBefore = transistorsParams[property.Name + trans.Name];

                                        if (!wasAlertBefore)
                                        {
                                            currentTime = DateTime.Now;
                                            WhenVolumenOverloaded(trans.Name, property.Name, currentTime, loggedUser);
                                        }
                                        transistorsParams[property.Name + trans.Name] = true;
                                    }
                                    else
                                    {
                                        transistorsParams[property.Name + trans.Name] = false;
                                    }
                                }
                                else //nie ma jsona
                                {
                                    maxValue = ap.MaxValue;
                                    minValue = ap.MinValue;

                                    if (currentValue > maxValue || currentValue < minValue)
                                    {
                                        var wasAlertBefore = transistorsParams[property.Name + trans.Name];

                                        if (!wasAlertBefore)
                                        {
                                            currentTime = DateTime.Now;
                                            WhenVolumenOverloaded(trans.Name, property.Name, currentTime, loggedUser);
                                        }
                                        transistorsParams[property.Name + trans.Name] = true;
                                    }
                                    else
                                    {
                                        transistorsParams[property.Name + trans.Name] = false;
                                    }
                                }
                            }
                        }
                    }
                }
                loggedUser = _loggedUser;
            }
        }

        public void WhenVolumenOverloaded(string turpineName, string parameterName, DateTime currentTime, UserDto loggedUser)
        {
            string definedUser = "";
            if (loggedUser == null)
            {
                definedUser = "N/A";
            }
            if (loggedUser != null)
            {
                definedUser = loggedUser.UserName;
            }
            
            var deviceInformation = new DeviceInformation
            {
                DeviceName = turpineName,
                ParameterNameWhenOverload = parameterName,
                OverloadDateTime = currentTime,
                LoggedUserInformation = definedUser
            };

            _deviceRepo.Add(deviceInformation);
            _deviceRepo.SaveChanges();
        }

        
    }
}
