﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuissnesLayer.Interfaces;
using BuissnesLayer.Services;
using DataLayer.Models;
using Ninject.Modules;
using PowerPlantProject;
using PowerPlantProject.Repository;

namespace BuissnesLayer.Modules
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGenericRepository<User>>().To<GenericRepository<User>>();
            Bind<IGenericRepository<DeviceInformation>>()
                .To<GenericRepository<DeviceInformation>>();
        }
    }
}
