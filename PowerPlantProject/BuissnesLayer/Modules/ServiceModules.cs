﻿using BuissnesLayer.Interfaces;
using BuissnesLayer.Services;
using Ninject.Modules;

namespace BuissnesLayer.Modules
{
    public class ServiceModules : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<IDeviceInformationDispatcher>().To<DeviceInformationDispatcher>();
            Bind<IDeviceInformationService>().To<DeviceInformationService>();
        }
    }
}