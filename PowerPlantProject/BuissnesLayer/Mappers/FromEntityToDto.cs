﻿using BuissnesLayer.DTOs;
using DataLayer.Models;
using PowerPlantProject;

namespace BuissnesLayer.Mappers
{
    public class FromEntityToDto
    {
        public static UserDto UserEntityModelToDto(User user)
        {
            if (user == null)
                return null;

            var userDto = new UserDto();
            userDto.Id = user.Id;
            userDto.UserName = user.UserName;
            userDto.Password = user.Password;
            userDto.IsAdmin = user.IsAdmin;

            return userDto;
        }
        public static DeviceInformationDto DeviceInformationEntityModelToDto(DeviceInformation deviceInformation)
        {
            if (deviceInformation == null)
                return null;
            var deviceInfoDto = new DeviceInformationDto();
            deviceInfoDto.Id = deviceInformation.Id;
            deviceInfoDto.DeviceName = deviceInformation.DeviceName;
            deviceInfoDto.OverloadDateTime = deviceInformation.OverloadDateTime;
            deviceInfoDto.ParameterNameWhenOverload = deviceInformation.ParameterNameWhenOverload;
            deviceInfoDto.LoggedUserInformation= deviceInformation.LoggedUserInformation;
            

            return deviceInfoDto;
        }
    }
}