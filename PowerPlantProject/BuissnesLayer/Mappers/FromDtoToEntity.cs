﻿using BuissnesLayer.DTOs;
using PowerPlantProject;

namespace BuissnesLayer.Mappers
{
    public class FromDtoToEntity
    {
        public static User UserDtoModelToEntity(UserDto userDto)
        {
            if (userDto == null)
                return null;

            var user = new User();
            user.Id = userDto.Id;
            user.UserName = userDto.UserName;
            user.Password = userDto.Password;
            user.IsAdmin = userDto.IsAdmin;

            return user;
        }
    }
}