using System.Linq;
using DataLayer.Interfaces;

namespace PowerPlantProject.Repository
{
    public interface IGenericRepository<T> where T : class, IEntity
    {
        void Add(T entity);
        T Get(int id);
        IQueryable<T> GetAll();
        void Update(T entity);
        void Delete(int id);
        void SaveChanges();
    }
}