﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Sockets;
using DataLayer.Interfaces;

namespace PowerPlantProject.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntity
    {
        private readonly Context _dbContext;
        private DbSet<T> DataSet => _dbContext.Set<T>();

        public GenericRepository(Context dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual void Add(T entity)
        {
            if (true)
            {
                DataSet.AddOrUpdate(entity);
            }
        }

        public virtual T Get(int id)
        {
            return GetAll().Single(i => i.Id == id);
        }

        public IQueryable<T> GetAll()
        {
            return DataSet;
        }

        public virtual void Update(T entity)
        {
            if (!DataSet.Local.Contains(entity))
            {
                DataSet.AddOrUpdate(entity);
                //_dbContext.Entry(entity).State = EntityState.Modified;
            }
        }

        public virtual void Delete(int id)
        {
            DataSet.Remove(Get(id));
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}