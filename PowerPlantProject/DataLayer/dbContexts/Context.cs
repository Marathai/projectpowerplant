﻿using System.Data.Entity;
using System.Configuration;
using DataLayer.Models;

namespace PowerPlantProject
{
    public class Context : DbContext
    {
        public Context() : base(GetConnectionString())
        { }

        public DbSet<User> UsersDbSet { get; set; }

        public DbSet<DeviceInformation> DeviceInformationDbSet { get; set; }

        private static string GetConnectionString()
        {
            string cs = ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
            return cs;
        }
    }
}