﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Interfaces;
using PowerPlantProject;

namespace DataLayer.Models
{
    public class DeviceInformation : IEntity
    {
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public string ParameterNameWhenOverload { get; set; }
        public DateTime OverloadDateTime { get; set; }
        public string LoggedUserInformation { get; set; }
    }
}
