﻿using System;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.Services;

namespace CliLayer
{
    public class LogIn : ILogIn
    {
        private IUserService _userService;

        public LogIn(IUserService userService)
        {
            _userService = userService;
        }

        public UserDto Run()
        {
            Console.WriteLine("Zaloguj sie by rozpoczac" +
                              "\npodaj login i haslo");
            
            var user = new UserDto();
            user.UserName = Console.ReadLine();
            user.Password = Console.ReadLine();

            if (_userService.DoesUserExist(user))
            {
                Console.WriteLine("Zalogowano!");
                return user;
            }
            Console.WriteLine("Użytkownik o podanej nazwie lub logienie nie istnieje!");
            return null;
        }
    }
}