﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace CliLayer.Modules
{
    public class ClientModules : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogIn>().To<LogIn>();
            Bind<IAddNewUser>().To<AddNewUser>();
            Bind<IPrintGeneratedEnergy>().To<PrintGeneratedEnergy>();
        }
    }
}
