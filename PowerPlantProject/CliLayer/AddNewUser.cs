﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.Services;

namespace CliLayer
{
    public class AddNewUser : IAddNewUser
    {
        private IUserService _userService;

        public AddNewUser(IUserService userService)
        {
            _userService = userService;
        }

        public void AddUser()
        {
            var newUser = new UserDto();
            Console.WriteLine("Wprowadz nazwe uzytkownika");
            newUser.UserName = Console.ReadLine();

            Console.WriteLine("Wprowadz haslo");
            newUser.Password = Console.ReadLine();

            newUser.IsAdmin = InputFormat.YesNoAnswer(
                "Czy uzytkownik jest administratorem?");

            _userService.AddUser(newUser);
        }
    }
}