using System;
using System.Linq.Expressions;
using System.Threading;
using BuissnesLayer;
using BuissnesLayer.Configuration;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.JsonManagment;
using BuissnesLayer.Services;
using PowerPlantDataProvider;

namespace CliLayer
{
    public class ProgramExecuter
    {
        private IUserService _userService;
        private ILogIn _logIn;
        private IDeviceInformationDispatcher _deviceInformationDispatcher;
        private IAddNewUser _addNewUser;
        private bool DoesJsonExists = false;
        private int _defaultRefreshInterval = 250;
        private double _defaultPercentage = 0;
        private IPrintGeneratedEnergy _printGeneratedEnergy;
        private IDeviceInformationService _deviceInformationService;

        public ProgramExecuter(IUserService userService, ILogIn logIn
            , IDeviceInformationDispatcher deviceInformationDispatcher
            , IAddNewUser addNewUser
            , IPrintGeneratedEnergy printGeneratedEnergy
            , IDeviceInformationService deviceInformationService)
        {
            _userService = userService;
            _logIn = logIn;
            _deviceInformationDispatcher = deviceInformationDispatcher;
            _addNewUser = addNewUser;
            _printGeneratedEnergy = printGeneratedEnergy;
            _deviceInformationService = deviceInformationService;
        }

        private UserDto _loggedUser;
        
        public void Execute()
        {
            //_addNewUser.AddUser();

            GetJsonConfigData();

            var thread = new Thread(() =>
            {
                _deviceInformationDispatcher.SaveOverloadedLoop(_defaultPercentage, _loggedUser);
            });
            thread.Start();

            Console.WriteLine("Witaj w programie PowerPlant");
            bool isOn = true;
            while (isOn)
            {
                var user = _logIn.Run();
                if(user == null)
                {
                    while (true)
                    {
                        string message = "Ponowić próbę logowania: ";
                        var userInput = InputFormat.YesNoAnswer(message);
                        if (userInput == false)
                        {
                            isOn = false;
                            
                            break;
                        }
                        if (userInput)
                        {
                            break;
                        }
                    }
                }
                if (user != null && _userService.DoesUserIsAdmin(user))
                {
                    _deviceInformationDispatcher.SetLoggedUser(user);
                    _loggedUser = user;
                    AdminMenu();
                }
                if (user != null && !_userService.DoesUserIsAdmin(user))
                {
                    _deviceInformationDispatcher.SetLoggedUser(user);
                    _loggedUser = user;
                    UserMenu();
                }
            }
        }

        private void UserMenu()
        {
            bool isOn = true;
            while (isOn)
            {
                Console.WriteLine("0 - Cofnij");
                Console.WriteLine("1 - Wyświetl aktualny stan urządzeń elektrowni");
                Console.WriteLine("2 - Zliczanie energii");
                Console.WriteLine("3 - Wyeksportuj plik z danymi o przekroczeniach");
                var userDecision = InputFormat.GetUserIntInputSafe();
                var userValidDecision = InputFormat.GetNumberOfOptionsDecision(userDecision, 4);
                if (userValidDecision == 0)
                {
                    isOn = false;
                }
                else if (userValidDecision == 1)
                {
                    CurrentDeviceStateData();
                }
                else if(userValidDecision == 2)
                {
                    _printGeneratedEnergy.PrintEnergyData(_defaultRefreshInterval);
                }
                else if(userValidDecision == 3)
                {
                    ExportOverloadData();
                }
            }
        }

        private void AdminMenu()
        {
            bool isOn = true;
            while (isOn)
            {
                Console.WriteLine("0 - Cofnij do menu logowania");
                Console.WriteLine("1 - Dodaj użytkownika");
                Console.WriteLine("2 - Usuń użytkownika");
                Console.WriteLine("3 - Opcje programu");
                Console.WriteLine("4 - Utwórz plik konfiguracyjny");
                var userDecision = InputFormat.GetUserIntInputSafe();
                var userValidDecision = InputFormat.GetNumberOfOptionsDecision(userDecision, 5);

                if (userValidDecision == 0)
                {
                    isOn = false;
                }
                else if (userValidDecision == 1 )
                {
                    NewUserAddition();
                }
                else if (userValidDecision == 2)
                {
                    bool exit = true;
                    DeleteUser(exit);
                    
                }
                else if (userValidDecision == 3)
                {
                    UserMenu();
                }
                else if (userValidDecision == 4)
                {
                    var configurationJsonInput = new ConfigurationJsonInput();
                    configurationJsonInput.ConfigurationFileGetInputAndSave();
                }
            }
        }

        private void ExportOverloadData()
        {
            Console.Write("Podaj okres OD (rok/miesiąc/dzień godz/min): ");
            var dateSince = InputFormat.GetValidDateWithMinutes();
            Console.Write("Podaj okres DO (rok/miesiąc/dzień godz/min): ");
            var dateTo = InputFormat.GetValidDateWithMinutes();
            Console.Write("Podaj nazwę pliku: ");
            var fileName = InputFormat.GetUserInputSafe();
            _deviceInformationService.ExportOverloadReport(dateSince, dateTo, fileName);
        }

        private void DeleteUser(bool exitCondition)
        {
            while (exitCondition)
            {
                Console.WriteLine("Wpisz nazwę/login użytkownika którego chcesz usunąć");
                string userToDelete = InputFormat.GetUserInputSafe();
                if (userToDelete == _loggedUser.UserName)
                {
                    Console.WriteLine("Nie można usunąć bieżącego użytkownika. Akcję przerwano.");
                    exitCondition = false;
                }
                else
                {
                    var userExist = _userService.DoesUserExistInDataBase(userToDelete);
                    if (userExist)
                    {
                        Console.Write("Wpisza hasło Administratora: ");
                        string inputedPassword = InputFormat.GetUserInputSafe();
                        bool validPassword = _userService.GetValidAdminPassword(inputedPassword, _loggedUser.Password);
                        if (validPassword)
                        {
                            bool deleteEffect = _userService.DeleteUser(userToDelete);
                            if (deleteEffect)
                            {
                                Console.WriteLine("Usunięto użytkownika");
                                exitCondition = false;
                            }
                            else
                            {
                                Console.WriteLine("Coś poszło nie tak");
                                exitCondition = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Wprowadzone hasło jest niepoprawne");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Użytkownik o podanej nazwie nie istnieje w bazie danych");
                        string message = "Ponowić próbę: ";
                        bool userContinueDecision = InputFormat.YesNoAnswer(message);
                        if (!userContinueDecision)
                        {
                            exitCondition = false;
                        }
                    }
                }
            }
        }

        private void CurrentDeviceStateData()
        {
            var printDevicesCurrentState = new PrintDevicesCurrentState();
            printDevicesCurrentState.PrintData(DoesJsonExists, _defaultPercentage, _defaultRefreshInterval);
        }

        private void NewUserAddition()
        {
            var newUser = new UserDto();
            Console.Write("Podaj Login nowego Użytkownika: ");
            do
            {
                var userName = InputFormat.GetUserInputSafe();
                var exist = _userService.DoesUserLoginExistInDb(userName);
                if (exist == false)
                {
                    newUser.UserName = userName;
                    break;
                }
                Console.WriteLine("Użytkownik o podanym loginie istnieje juz w bazie danych! Wybierz inny login: ");
            } while (true);

            Console.Write("Podaj hasło nowego Użytkownika: ");
            newUser.Password = InputFormat.GetUserInputSafe();
            string message = "Czy chcesz nadać nowemu użytkownikowi status Administratora: ";
            newUser.IsAdmin = InputFormat.YesNoAnswer(message);
            _userService.AddUser(newUser);
        }

        private void GetJsonConfigData()
        {
            var json = new JsonService<ConfigurationClass>();
            try
            {
                var configurations = json.GetDataFromJson();
                _defaultPercentage = configurations.PercentOfVarationFromTypicalValue;
                _defaultRefreshInterval = configurations.RefreshDataInterval;
            }
            catch (Exception e)
            {
                DoesJsonExists = false;
            }
        }
    }
}
