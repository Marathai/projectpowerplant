using BuissnesLayer.DTOs;

namespace CliLayer
{
    public interface ILogIn
    {
        UserDto Run();
    }
}