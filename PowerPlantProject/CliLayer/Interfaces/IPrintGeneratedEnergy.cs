﻿namespace CliLayer
{
    public interface IPrintGeneratedEnergy
    {
        void PrintEnergyData(int timePeriod);
    }
}