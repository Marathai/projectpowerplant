﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BuissnesLayer.Modules;
using CliLayer.Modules;
using Ninject;

namespace CliLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new RepositoryModule(), new ServiceModules(), new ClientModules());
            kernel.Get<ProgramExecuter>().Execute();
        }
    }
}
