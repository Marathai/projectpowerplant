﻿using System;
using System.Data.Entity.Migrations.Model;
using BuissnesLayer;
using PowerPlantDataProvider;

namespace CliLayer
{
    public class PrintGeneratedEnergy : IPrintGeneratedEnergy
    {
        public void PrintEnergyData(int timePeriod)
        {
            double energyGeneratedInTotal = 0;
            var counter = new CountGeneratedEnergy();

            while (true)
            {
                foreach (var turbine in PowerPlant.Instance.Turbines)
                {
                    energyGeneratedInTotal = counter.EnergyGenerated(timePeriod, energyGeneratedInTotal,
                        turbine.CurrentPower.CurrentValue);
                    Console.WriteLine(turbine.Name +" "+ turbine.CurrentPower.CurrentValue);
                }

                Console.WriteLine("Energy generated: "+energyGeneratedInTotal);

                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape)
                    break;

                RefreshConsole.Clear(timePeriod, 4);
            }
        }
    }
}