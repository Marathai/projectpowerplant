﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace CliLayer
{
    public static class InputFormat
    {
        public static string LoginOperations(string message)
        {
            Console.WriteLine(message);
            string operation;

            while (true)
            {
                operation = Console.ReadLine();
                if (operation == "A" || operation == "L")
                    break;

                Console.WriteLine("Wybierz poprawna akcje");
            }
            return operation;
        }

        public static bool YesNoAnswer(string message)
        {
            Console.WriteLine(message);
            string operation;

            while (true)
            {
                operation = Console.ReadLine();
                if (operation == "yes")
                    return true;
                if (operation == "no")
                    return false;

                Console.WriteLine("Wybierz poprawna akcje");
            }
        }

        public static string GetUserInputSafe()
        {
            string userDecision;
            do
            {
                userDecision = Console.ReadLine();
                if (!string.IsNullOrEmpty(userDecision))
                {
                    return userDecision;
                }
                Console.Write("Wprowadzono nie prawidłowy format! Spróbuj jeszcze raz: ");
            } while (true);
        }

        public static int GetUserIntInputSafe()
        {
            string userDecision;
            int outputValue = 0;
            do
            {
                userDecision = Console.ReadLine();
                bool validInt = int.TryParse(userDecision, out outputValue);
                if (validInt)
                {
                    return outputValue;
                }
                Console.Write("Wprowadzono nieprawidłowy format! Spróbuj jeszcze raz: ");
            } while (true);
        }

        public static int GetNumberOfOptionsDecision(int userInput, int numberOfMenuOptions)
        {
            while (userInput < 0 && userInput > numberOfMenuOptions)
            {
                Console.Write("Wprowadzono nieprawidłowy format! Spróbuj jeszcze raz: ");
                userInput = GetUserIntInputSafe();
            }
            return userInput;
        }

        public static double GetDouble(string message)
        {
            Console.WriteLine(message);

            while (true)
            {
                var operation = Console.ReadLine();
                try
                {
                    return double.Parse(operation);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Podaj liczbe");
                }
            }
        }

        public static int GetInt(string message)
        {
            Console.WriteLine(message);

            while (true)
            {
                var operation = Console.ReadLine();
                try
                {
                    return Int32.Parse(operation);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Podaj liczbe calkowita");
                }
            }
        }

        public static DateTime GetDateTime(string message)
        {
            Console.WriteLine(message);

            while (true)
            {
                var operation = Console.ReadLine();
                try
                {
                    return DateTime.Parse(operation);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Podaj poprawny format daty wraz z minutą");
                }
            }
        }

        public static DateTime GetValiDateTime()
        {
            DateTime output;
            do
            {
                var input = Console.ReadLine();
                bool tryParse = DateTime.TryParse(input, out output);
                if (tryParse)
                {
                    return output;
                }
            } while (true);
        }

        public static DateTime GetValidDateWithMinutes()
        {
            Regex reg = new Regex(@"^[\d]{4}\/[\d]{2}\/[\d]{2}\s[\d]{2}:[\d]{2}$");
            do
            {
                DateTime output;
                var input = GetUserInputSafe();
                bool tryParse = DateTime.TryParse(input, out output);
                if (tryParse && reg.IsMatch(input))
                {
                    return output;
                }
                Console.Write("Coś poszło nie tak! Spróbuj raz jeszcze: ");
            } while (true);
        }
    }
}