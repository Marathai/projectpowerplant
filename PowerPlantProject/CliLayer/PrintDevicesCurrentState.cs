using System;
using System.ComponentModel;
using BuissnesLayer;
using PowerPlantDataProvider;

namespace CliLayer
{
    public class PrintDevicesCurrentState
    {
        public void PrintData(bool json, double percentage, int timePeriod)
        {
            while (true)
            {
                Console.WriteLine(PowerPlant.Instance.Name + "\n");
                PrintTransformatorsData(json, percentage);
                PrintCauldronsData(json, percentage);
                PrintTurbinesData(json, percentage);

                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape)
                    break;
                RefreshConsole.Clear(timePeriod, 35);
            }
        }

        public void PrintTurbinesData(bool json, double percentage)
        {
            double maxValue;
            double minValue;

            foreach (var x in PowerPlant.Instance.Turbines)
            {
                Console.WriteLine(x.Name);

                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(x))
                {
                    Console.Write(property.Name + " ");

                    var value = property.GetValue(x);
                    if (value.GetType() == typeof(AssetParameter))
                    {
                        AssetParameter ap = (AssetParameter)value;
                        if (json == false)
                        {
                            maxValue = ap.MaxValue;
                            minValue = ap.MinValue;
                        }
                        else
                        {
                            maxValue = ap.TypicalValue * (1 + percentage);
                            minValue = ap.TypicalValue * (1 - percentage);
                        }

                        MarkIfValueIfOutsideRange(ap.CurrentValue, minValue,
                            maxValue);
                        Console.Write(" " + ap.Unit + " \n");
                    }
                }
                Console.WriteLine(" ");
            }
        }

        public void PrintCauldronsData(bool json, double percentage)
        {
            double maxValue;
            double minValue;

            foreach (var x in PowerPlant.Instance.Cauldrons)
            {
                Console.WriteLine(x.Name);

                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(x))
                {
                    Console.Write(property.Name + " ");

                    var value = property.GetValue(x);
                    if (value.GetType() == typeof(AssetParameter))
                    {
                        AssetParameter ap = (AssetParameter)value;

                        if (json == false)
                        {
                            maxValue = ap.MaxValue;
                            minValue = ap.MinValue;
                        }
                        else
                        {
                            maxValue = ap.TypicalValue * (1 + percentage);
                            minValue = ap.TypicalValue * (1 - percentage);
                        }

                        MarkIfValueIfOutsideRange(ap.CurrentValue, minValue,
                            maxValue);
                        Console.Write(" " + ap.Unit + " \n");
                    }
                }
                Console.WriteLine(" ");
            }
        }

        public void PrintTransformatorsData(bool json, double percentage)
        {
            double maxValue;
            double minValue;

            foreach (var x in PowerPlant.Instance.Transformators)
            {
                Console.WriteLine(x.Name);

                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(x))
                {
                    Console.Write(property.Name + " ");

                    var value = property.GetValue(x);
                    if (value.GetType() == typeof(AssetParameter))
                    {
                        AssetParameter ap = (AssetParameter)value;

                        if (json == false)
                        {
                            maxValue = ap.MaxValue;
                            minValue = ap.MinValue;
                        }
                        else
                        {
                            maxValue = ap.TypicalValue * (1 + percentage);
                            minValue = ap.TypicalValue * (1 - percentage);
                        }

                        MarkIfValueIfOutsideRange(ap.CurrentValue, minValue,
                            maxValue);

                        Console.Write(" " + ap.Unit + " \n");
                    }
                }
                Console.WriteLine(" ");
            }
        }

        public void MarkIfValueIfOutsideRange(double currentValue, double minValue, double maxValue)
        {
            if (currentValue > maxValue || currentValue < minValue)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(currentValue);
                Console.ResetColor();
            }
            else
                Console.Write(currentValue);
        }
    }
}