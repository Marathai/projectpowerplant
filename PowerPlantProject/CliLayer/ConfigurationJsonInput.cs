﻿using System;
using BuissnesLayer.Configuration;
using BuissnesLayer.JsonManagment;

namespace CliLayer
{
    public class ConfigurationJsonInput
    {
        public void ConfigurationFileGetInputAndSave()
        {
            var configObj = new ConfigurationClass();

            Console.WriteLine("Ustawienia fonfiguracji \n");

            configObj.PercentOfVarationFromTypicalValue = 
                InputFormat.GetDouble("Podaj dopuszczalne odchylenie od typowej wartości procentach");

            configObj.RefreshDataInterval = InputFormat.GetInt("Podaj częstotliwość odświeżania w ms");

            var save = new JsonService<ConfigurationClass>();
            save.SaveDataToJson(configObj, "settings");
        }
    }
}