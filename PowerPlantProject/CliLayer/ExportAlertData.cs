﻿using System;
using System.Collections.Generic;
using BuissnesLayer.DTOs;
using BuissnesLayer.Interfaces;
using BuissnesLayer.JsonManagment;

namespace CliLayer
{
    public class ExportAlertData
    {
        private IDeviceInformationService _deviceService;

        public ExportAlertData (IDeviceInformationService deviceService)
        {
            _deviceService = deviceService;
        }

        public void ExportationToJson()
        {
            var alertDataList = new List<DeviceInformationDto>();

            Console.WriteLine("Zapis Informacji o alertach do pliku json");

            var initialTime = InputFormat.GetDateTime("Podaj poczatkowa date z dokladnoscia do minuty:");
            var finalTime = InputFormat.GetDateTime("Podaj koncowa date z dokladnoscia do minuty:");
            Console.WriteLine("Podaj jak chcesz nazwac plik z danymi");
            var fileName = Console.ReadLine();

            alertDataList = _deviceService.GetAllAlerts(initialTime, finalTime);

            var save = new JsonService<List<DeviceInformationDto>>();
            save.SaveDataToJson(alertDataList, fileName);
        }
    }
}